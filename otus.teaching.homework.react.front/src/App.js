import "./App.css";
import React, { Component } from "react";
import { Route } from "react-router";
import { Layout } from "./components/Layout";
import { Home } from "./components/Home";
import { Weather } from "./components/Weather";

class App extends Component {
  render() {
    return (
      <Layout>
        <Route exact path="/" component={Home} />
        <Route path="/weather" component={Weather} />
      </Layout>
    );
  }
}

export default App;
